import {hoverTarget} from "./scripts/hover-target.js";
import {forceDCSave} from "./scripts/save-roller.js";
import {AbilityTemplate} from "./scripts/abilityTemplate.js";
import {showResults} from "./scripts/show-results.js";

//Clamp Function, pretty handy:
Math.clamp = function (number, min, max) {
    return Math.max(min, Math.min(number, max));
}

Hooks.on("createChatMessage", (message) => {
    if (game.user._id === game.users.entities.find(u => u.isGM)._id || game.settings.get("pf2st", "PlayersCanQuickRoll")) {
        if (game.combat === null || (game.user._id === game.users.entities.find(u => u.isGM)._id) || !game.settings.get("pf2st", "RollOnTurn") || (game.combat.combatant.players[0].data._id === game.user._id)) {
            if (message.data.content.includes('<button>Save DC')) createDCButtonListener(message);
            if (!message.data) return true;
            if (!message.data.flavor) return true;
           // if (message.data.flavor.includes('Attack Roll') && message.data.user === game.user.data._id) compareAttacks(message);
           // if (message.data.flavor.includes('Strike:') && message.data.user === game.user.data._id) compareAttacks(message);
        }
    }
});

// Handle-socket
Hooks.on('ready', () => {
    console.log("Registering...")
    game.socket.on('module.pf2st', async (data) => {
        if (game.user.isGM && data) showResults(data);
      });
    })

//Pre-Create:
Hooks.on("preCreateChatMessage", (data) => {
    //if (data.content.includes('Damage')) augmentDamageButton(data);
    if (data.content.includes(`<button data-action="save`)) createDCButton(data);
});


Hooks.on('ready', () => {
    [
        {
            name: "markSuccesses",
            hint: "Tokens are marked with a degree of success. Can be selected with macros included in compendium.",
            scope: "world",
            default: false,
            type: Boolean
        }

    ].forEach((setting) => {
        let options = {
            name: setting.name,
            hint: setting.hint,
            scope: setting.scope,
            config: true,
            default: setting.default,
            type: setting.type,
        };
        game.settings.register("pf2st", setting.name, options);
    });

});




//Save DC Button Creation:
function createDCButton(data) {
    let content = data.content;
    content = content.split('\n');
    for (let index = 0; index < content.length; index++) {
        const element = content[index];
        if (element.includes("Save DC")) {
            let dtype = 'will';
            if (element.includes("Reflex")) {
                dtype = "reflex";
            }
            if (element.includes("Will")) {
                dtype = "will";
            }
            if (element.includes("Fortitude")) {
                dtype = "fortitude";
            }

    if(game.settings.get("pf2st", "markSuccesses"))        {
            content[index] = content[index].replace(
                `<button data-action="save`, '<button data-action="saveDC');
            content[index] = content[index].replace("</span>", "</button>");
            }
        }
    }
    content = content.join('\n');
    data.content = content;

    if(data.content.includes("Area")){
        createTemplateButton(data);
    }
}

Hooks.on('renderChatLog', (log, html) => {
    
    hoverTarget();

    html.on('click', '.card-buttons button', (ev) => {
        let cardID = $(ev)[0].originalEvent.path[2].dataset.itemId
        const button = $(ev.currentTarget);
  console.log(ev)



        const dc = ev.currentTarget.innerText.match(/\d+/g)
        const action = button.attr('data-action');
        const actionSave = button.attr('data-save')
        if (action.includes('template')) createTemplate(button);
        if (action.includes('saveDC'))
        
        {
            let cardActorid = ev.currentTarget.parentElement.parentElement.attributes[3].value    
        forceDCSave(actionSave, dc, cardID, ev, cardActorid);
        }
    });
});



//Creating of the template buttons in chat log:



function createTemplateButton(data) {
    let content = data.content;
    content = content.split('\n');

    let range = 0;
    let shape = "ERROR";

    for (let index = 0; index < content.length; index++) {
        const element = content[index];
        if (element.includes("Area:")) {
            range = content[index].match(/\d+/g)
            if(element.includes("Burst")){
                shape="circle";
            }
            if(element.includes("Cone")){
                shape="cone";
            }
            if(element.includes("Emanation")){
                shape="rect";
            }
            if(element.includes("Line")){
                shape="ray";
            }
        }
    }
    content = content.join('\n');
    data.content = content;
    data.content+="<div class='card-buttons'><button data-action='template' class='templateButton' data-area='"+range+"' data-shape='"+shape+"'>Place a "+range+"ft "+shape+"</button></div>";
}

var pf2stGhostTemplate = null;

function createTemplate(button){
    const range = (button[0].attributes['data-area'].value);
    const type = (button[0].attributes['data-shape'].value);
    console.log(type);

    if(pf2stGhostTemplate!=null) pf2stGhostTemplate.kill();
    const templateData = {
        t: type,
        user: game.user._id,
        x: 1000,
        y: 1000,
        direction: 180,
        angle: 90,
        distance: range,
        borderColor: "#FF0000",
        fillColor: "#FF3366",
    };
    if(type==="ray"){
        templateData.width=5;
    }
    if(type==="rect"){
        templateData.direction=45;
        templateData.width=range;
        templateData.distance=Math.hypot(range,range);
    }

    pf2stGhostTemplate = new AbilityTemplate(templateData);
    pf2stGhostTemplate.drawPreview();
}
