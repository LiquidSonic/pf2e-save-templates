import { critCheck } from "./crit-test.js";
import { showResults } from "./show-results.js";

export async function forceDCSave(action, dc, id, event, cardActorid) {
    // Liquid Sonic Awesome Codes //
    // Liquidsonic's build dice roll - "estabalish first token in selected array. Establish the Spell traits found from card. Declare Spell Rolls "



    let tokens = canvas.tokens.placeables.find(token => token.actor.id == cardActorid)

    // Functionality needs to be swapped out
    let damageMacro = game.macros.entities.find(m => m.name === "Damage Macro")


console.log(tokens)
    let spellId = tokens.actor.data.items.find(item => item._id === id).data.damage.value

    let spellTraits = tokens.actor.data.items.find(item => item._id === id).data.traits.value
    let spellRoll

    // Liquid Sonic Awesome Codes //



 /*   if (spellId) {
        spellRoll = new Roll(spellId).roll();

        spellRoll.toMessage({
            flavor: "Spell Damage",

            rollMode: "public"
        })

    }*/

    let saveName = 'error';

    if (action.includes('reflex')) {
        saveName = 'Reflex';
    }
    if (action.includes('will')) {
        saveName = 'Will';
    }
    if (action.includes('fortitude')) {
        saveName = 'Fortitude';
    }
    if (saveName === 'error') {
        ui.notifications.error("Invalid save type. Make sure this particular spell has a save type in its config, not all start with one.")
        return;
    }

    // Pretty sure this is useless now
    const parts = ['@mod'];
    const flavor = `${CONFIG.PF2E.saves[saveName]} Save Check`;
    //

    // Parse the dc from the spellcard
    dc = parseInt(dc[0]);
    let message = `<div><h3 style='border-bottom: 3px solid black'>${saveName} Saving Throw</h3></div>`

    let loopCount = 0;

    // Not entirely sure what this does.. believe it waits until all targets are mapped? Don't want to mess with this tbh

    const messages = await Promise.all([...canvas.tokens.controlled].map(async t => {

        t.actor.data.data.saves[action].roll(event, spellTraits, (result) => {





            loopCount++
            //Success step meaning:
            // 3 = Critical
            // 2 = success
            // 1 = failure
            // 0 = critical failure
            let successStep = -1;

            //Rolling the save from the target actor. WILL NOT WORK OF QUICKROLLS ARE NOT ENABLED

            let roll = result
            let text = "";

            //Should add this to a separate file like a DB. Probably a Switch case.

            if (!roll) {
                return '';
            }

            //Setting the basic roll from its comparison with DC:
            if (roll._total >= dc) {
                successStep = 2;
            } else {
                successStep = 1;
            }

            //applying effects of crits and naturals:
            successStep += critCheck(roll, dc);
            successStep = Math.clamp(successStep, 0, 3);

            let key = Array.from(game.messages)

            let newMessage;

            let keyID = key[Array.from(game.messages).length - 1].id



            let successBy = roll._total - dc;
            switch (successStep) {
                case 0:
                    newMessage = key[Array.from(game.messages).length - 1].data.flavor.replace(`</b>`, `💔 Crit Failed, Doubled Effect</b> ${text}`)
                    t.toggleOverlay("/modules/pf2st/images/save_criticalfail.png")
                    if (spellId) {
                        message += ` Takes ${Math.floor(spellRoll.total * 2)} damage.`
                   //      damageMacro.execute(`${t.data._id}`, `${Math.floor(spellRoll.total * 2)}`);
                        t.actor.update({ 'data.attributes.hp.value': t.actor.data.data.attributes.hp.value - (Math.floor(spellRoll.total * 2)) })


                    }

                    break;
                case 1:
                    newMessage = key[Array.from(game.messages).length - 1].data.flavor.replace(`</b>`, `❌ Fail, Full Effect</b> ${text}`)
                    t.toggleOverlay("/modules/pf2st/images/save_fail.png")
                    break;
                case 2:

                    newMessage = key[Array.from(game.messages).length - 1].data.flavor.replace(`</b>`, `✔️ Success, Half Effect</b> ${text}`)
                    t.toggleOverlay("/modules/pf2st/images/save_success.png")
                    break;
                case 3:
                    newMessage = key[Array.from(game.messages).length - 1].data.flavor.replace(`</b>`, `💥 Crit Success, No Effect</b> ${text}`)
                    t.toggleOverlay("/modules/pf2st/images/save_criticalsuccess.png")
                    break;

            }


            game.messages.entities.find(message => message.id === keyID).update({ flavor: newMessage })
            /*
                    if (loopCount == game.user.targets.size) {
                        let chatData = {
                            user: game.user._id,
                            content: message
                        }
                        showResults(chatData);
                    }
            */
        })

    }));

}
