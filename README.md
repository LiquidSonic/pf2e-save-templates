Pathfinder 2e Save Templates

Liquidsonic's Manifest: https://gitlab.com/LiquidSonic/pf2e-save-templates/raw/master/module.json

Remove redundant functionality.

Attack roll comparisons handled by PF2E Core.

Save results handled by PF2E Core.

Added Functionlity for selecting targets based on save results and visibily showing this.

![Preview](/uploads/17f20a6826e7e349f1d3c2c1f82249d8/Preview.mp4)
